var CCKFlashcard = CCKFlashcard || {};

CCKFlashcard.doClick = function(name) {
  // Show num and hide all chunks that are not num
  $("." + name + "_show").hide();
  $("." + name + "_hide").show();
}

$(document).ready(function(){
  // Click handler
  $('.cck_flashcard').mouseover(function() {
    linkName = $(this).attr('name');
    $("." + linkName + "_show").hide();
    $("." + linkName + "_hide").show();
  });
    $('.cck_flashcard').mouseout(function() {
    linkName = $(this).attr('name');
    $("." + linkName + "_hide").hide();
    $("." + linkName + "_show").show();
  });
});
